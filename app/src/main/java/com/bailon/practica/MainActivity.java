package com.bailon.practica;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.*;

import com.bailon.practica.R;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MainActivity extends Activity {


    Button escribir, leer;
    EditText archivo, cajaCedula, cajaNombres, cajaApellidos;
    TextView mostrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        escribir = (Button)findViewById(R.id.btnescribir);
        leer = (Button)findViewById(R.id.btnleer);
        cajaApellidos = (EditText)findViewById(R.id.txtapellidos);
        cajaNombres = (EditText)findViewById(R.id.txtnombres);
        cajaCedula = (EditText)findViewById(R.id.txtcedula);
        mostrar = (TextView)findViewById(R.id.txtDatos);

        escribir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    OutputStreamWriter out = new OutputStreamWriter(openFileOutput("archivo.txt", Context.MODE_PRIVATE));
                    out.write(cajaCedula.getText().toString() + " , " + cajaApellidos.getText().toString() + " , " + cajaNombres.getText().toString() + " , " );
                    Log.e("Archivo MI","Archivo guardado en la memoria interna");
                    out.close();
                }catch (Exception ex){
                    Log.e("Archivo MI","Error al guardar en la memoria interna");
                }
            }
        });

        leer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    BufferedReader read = new BufferedReader(new InputStreamReader(openFileInput("archivo.txt")));
                    String texto = read.readLine();
                    mostrar.setText(texto);
                    read.close();
                }catch (Exception ex){
                    Log.e("Archivo MI","Error al leer el archivo de la memoria interna");
                }
            }
        });


    }

}

